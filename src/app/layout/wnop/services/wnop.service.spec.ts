import { TestBed } from '@angular/core/testing';

import { WnopService } from './wnop.service';

describe('WnopService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WnopService = TestBed.get(WnopService);
    expect(service).toBeTruthy();
  });
});
