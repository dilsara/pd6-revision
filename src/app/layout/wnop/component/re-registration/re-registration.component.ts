import { Component, OnInit, ViewChild } from '@angular/core';
import { PersonalInformationComponent } from '../common/personal-information/personal-information.component';
import { FormGroup } from '@angular/forms';
import { ServiceInformationComponent } from '../common/service-information/service-information.component';
import { PaymentIformationComponent } from '../common/payment-iformation/payment-iformation.component';
import { DependantInformationComponent } from '../common/dependant-information/dependant-information.component';
import { WnopService } from '../../services/wnop.service';
import { ProfileDetailsComponent } from '../common/profile-details/profile-details.component';
import { profileModel } from '../../model/profileModel';
import { DependantsInfoModel } from '../../model/dependant_info.model';
import { MasterDataService } from 'src/app/services/master-data.service';
import { PersonalFileInputComponent } from '../common/personal-file-input/personal-file-input.component';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-re-registration',
  templateUrl: './re-registration.component.html',
  styleUrls: ['./re-registration.component.scss']
})
export class ReRegistrationComponent implements OnInit {

  isLinear = true;
  isEdit = false;
  isNewRegister: any = true;
  form: FormGroup;
  model: any;
  response;
  isDependentFormValied: boolean;
  dependantModel: DependantsInfoModel;

  @ViewChild(PersonalFileInputComponent) documentUploadForm: PersonalFileInputComponent;
  @ViewChild(PersonalInformationComponent) personalInfoForm: PersonalInformationComponent;
  @ViewChild(ServiceInformationComponent) serviceInfoForm: ServiceInformationComponent;
  @ViewChild(DependantInformationComponent) dependantInfoForm: DependantInformationComponent;
  @ViewChild(PaymentIformationComponent) paymentInfoForm: PaymentIformationComponent;
  @ViewChild(ProfileDetailsComponent) profileDetail: ProfileDetailsComponent;

  constructor(
    private service: WnopService,
    private mdService: MasterDataService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.activatedRoute.params.subscribe(
      params => {
        this.isNewRegister = params['value'];
      }
    );
  }

  ngOnInit() {
    this.isDependentFormValied = true;
  }

  documentUploadStepperNext(stepper) {
    if (this.documentUploadForm.form.invalid) {
      this.documentUploadForm.isClickNext = true;
    } else {
      stepper.next();
    }
  }

  personalInfoStepperNext(stepper) {
    if (this.personalInfoForm.form.invalid) {
      this.personalInfoForm.isClickNext = true;
    } else {
      this.profileDetail.setModel(this.getModel());
      stepper.next();
    }
  }

  ServiceInfoStepperNext(stepper) {
    if (this.serviceInfoForm.form.invalid) {
      this.serviceInfoForm.isClickNext = true;

    } else {
      this.profileDetail.setModel(this.getModel());
      stepper.next();
    }
  }

  DependantInfoStepperNext(stepper) {
    let DependentDetail = this.dependantInfoForm.getModel();
    this.isDependentFormValied = true;
    if (DependentDetail.marital_status == 1 && DependentDetail.spouses.length == 0) {
      this.dependantInfoForm.isMarried = true;
      this.isDependentFormValied = false;
    } else {
      this.profileDetail.setModel(this.getModel());
      stepper.next();
    }
  }

  getModel() {
    let profile = {
      personalInfo: this.personalInfoForm.getModel(),
      serviceInfo: this.serviceInfoForm.getModel(),
      dependants: this.dependantInfoForm.getModel().dependants,
      spouses: this.dependantInfoForm.getModel().spouses,
      marital_status: this.dependantInfoForm.getModel().marital_status,
      wnopInfo: null
    }
    return profile;
  }

  setModel(model: profileModel) {
    model.serviceInfo.wnop_number = model.wnopInfo.old_wnop_number;
    this.personalInfoForm.setModel(model.personalInfo),
      this.serviceInfoForm.setModel(model.serviceInfo)
    let dependantModel: DependantsInfoModel = {
      marital_status: model.marital_status,
      spouses: model.spouses,
      dependants: model.dependants
    }
    this.dependantInfoForm.setModel(dependantModel)
  }

  submit() {

    let profile = {
      personalInfo: this.personalInfoForm.getModel(),
      serviceInfo: this.serviceInfoForm.getModel(),
      dependants: this.dependantInfoForm.getModel().dependants,
      spouses: this.dependantInfoForm.getModel().spouses,
      marital_status: this.dependantInfoForm.getModel().marital_status,
      wnopInfo: {
        person_id: 0,
        wnop_number: 0,
        old_wnop_number: this.serviceInfoForm.getModel().wnop_number,
        status: 100,
        create_by: localStorage.getItem("userId"),
        approved_by: localStorage.getItem("userId")
      }
    }
    if (!this.isEdit) {
      this.service.createProfile(profile).subscribe(
        response => {
          let payload = {
            "mime/type": "image/jpeg",
            "valueImage": profile.personalInfo.image.replace(/^data:[a-z]+\/[a-z]+;base64,/, "")
          }
          this.mdService.SAVE_IN_FILE_SERVER("13", "img-" + response.personalInfo.id, payload)
            .subscribe(_res => {
              //If new register document should be uploade
              if (this.isNewRegister) {
                let nicBase64 = {
                  "mime/type": "image/jpeg",
                  "valueImage": this.documentUploadForm.getModel().nic.replace(/^data:[a-z]+\/[a-z]+;base64,/, "")
                }
                this.mdService.SAVE_IN_FILE_SERVER("13", "nic-" + response.personalInfo.id, nicBase64)
                  .subscribe(_res => {
                    let AppointmentletterBase64 = {
                      "mime/type": "image/jpeg",
                      "valueImage": this.documentUploadForm.getModel().appointmentletter.replace(/^data:[a-z]+\/[a-z]+;base64,/, "")
                    }
                    this.mdService.SAVE_IN_FILE_SERVER("13", "AL-" + response.personalInfo.id, AppointmentletterBase64)
                      .subscribe(_res => {
                        //
                      })
                  },
                    err => alert(err.error.message)
                  )
              }
            },
              err => alert(err.error.message)
            )
          this.response = response;
          alert('Profile Details Saved Successfully');
        },
        err => alert(err.error.message)
      )
    }

    else if (this.isEdit) {
      this.service.updateProfile(profile).subscribe(
        response => {
          let payload = {
            "mime/type": "image/jpeg",
            "valueImage": profile.personalInfo.image.replace(/^data:[a-z]+\/[a-z]+;base64,/, "")
          }
          this.mdService.SAVE_IN_FILE_SERVER("13", "img-" + response.personalInfo.id, payload)
            .subscribe(_res => {
              this.response = response;
              alert('Profile Details Update Successfully');
            },
              err => alert(err.error.message)
            )

        },
        err => alert(err.error.message)
      )
    }
    this.router.navigate(['/wnop']);
  }






}