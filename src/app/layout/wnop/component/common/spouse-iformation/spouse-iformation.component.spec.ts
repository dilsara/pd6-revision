import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpouseIformationComponent } from './spouse-iformation.component';

describe('SpouseIformationComponent', () => {
  let component: SpouseIformationComponent;
  let fixture: ComponentFixture<SpouseIformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpouseIformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpouseIformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
