import { Component, OnInit, ViewChild } from '@angular/core';
import { WnopService } from '../../../services/wnop.service'; 
import { profileModel } from '../../../model/profileModel';
import { PersonalDetailComponent } from '../personal-detail/personal-detail.component';
import { ServiceDetailComponent } from '../service-detail/service-detail.component';
import { DependantDetailComponent } from '../dependant-detail/dependant-detail.component';
import { MasterDataService } from 'src/app/services/master-data.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.scss']
})
export class ProfileDetailsComponent implements OnInit {

  form: FormGroup; 
  model= null; 

  @ViewChild(PersonalDetailComponent) personalDetail :PersonalDetailComponent;
  @ViewChild(ServiceDetailComponent) serviceDetail :ServiceDetailComponent;
  @ViewChild(DependantDetailComponent) dependantDetail :DependantDetailComponent;

  constructor(
  private service: WnopService,     
  private mdService :MasterDataService,
  private formBuilder: FormBuilder
  ) {     
  }

  ngOnInit() {
    
  }

  // changeModel(id: number) {
  //   this.service.get(id).subscribe(
  //     response => { 
  //       this.mdService.GET_PENSIONER_PHOTO("img-" +id).subscribe(
  //         image => {
  //           response.personalInfo.image = "data:"+ image["value"][0]["mime/type"] + ";base64," + image["value"][0]["valueImage"]
  //           this.setModel(response) ; 
  //         })  
  //       this.setModel(response) ;        
  //     },
  //     error => {
  //       console.log(error);
  //     }
  //   );
  // }  

  setModel(model:profileModel) {
    this.model = model;
    this.personalDetail.setModel(model.personalInfo);
    this.serviceDetail.setModel(model.serviceInfo, model.wnopInfo)
    this.dependantDetail.setModel(model.dependants,model.spouses,model.marital_status)
  }

  initFormChangeHooks() {

  }

}
