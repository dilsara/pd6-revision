import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SpouseInfoTblModel } from '../../../model/spouse_infoTb.model'; 

@Component({
  selector: 'app-add-dependant',
  templateUrl: './add-dependant.component.html',
  styleUrls: ['./add-dependant.component.scss']
})
export class AddDependantComponent implements OnInit {

  form: FormGroup;
  maxDate: Date;

  constructor(
    private formBuilder: FormBuilder, 
    private dialogRef: MatDialogRef<AddDependantComponent>,
    @Inject(MAT_DIALOG_DATA) public spouses: SpouseInfoTblModel[],
  ) { 
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate() - 1);
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      id : [0],
      person_id: [0],
      fullName: null,
      relation: [''], 
      dob: [''], 
      nic: [''],
      differentlyAabled: [false, Validators.required],
      guardian: [''],
      guardian_id :[0],
    });

    this.initFormConfigurationHooks();
  }

  close() {
    this.dialogRef.close();
  }

  saveAndClose() {
    if(this.form.valid) {
      this.dialogRef.close(this.form);
    
    } else {
      //this.alert.show('Please Fill the Form Correctly');
    }
  }

  initFormConfigurationHooks() {
    if(this.spouses.length == 0) {
      this.form.get('guardian').disable();  
    }
  }
}
