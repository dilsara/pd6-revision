import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DependantInformationComponent } from './dependant-information.component';

describe('DependantInformationComponent', () => {
  let component: DependantInformationComponent;
  let fixture: ComponentFixture<DependantInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DependantInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DependantInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
