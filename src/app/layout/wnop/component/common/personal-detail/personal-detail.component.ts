import { Component, OnInit } from '@angular/core';
import { PersonalInfoModel } from '../../../model/personal_info.model';

@Component({
  selector: 'app-personal-detail',
  templateUrl: './personal-detail.component.html',
  styleUrls: ['./personal-detail.component.scss']
})
export class PersonalDetailComponent implements OnInit {

  constructor() { }

  model :PersonalInfoModel = null; 

  ngOnInit() { 
  }

  setModel(model: PersonalInfoModel) {
    this.model = model;
  }
}
