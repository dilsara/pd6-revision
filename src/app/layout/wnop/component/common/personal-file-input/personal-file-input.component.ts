import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SearchPersonComponent } from '../search-person/search-person.component';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-personal-file-input',
  templateUrl: './personal-file-input.component.html',
  styleUrls: ['./personal-file-input.component.scss']
})
export class PersonalFileInputComponent implements OnInit {

  form: FormGroup;
  isClickNext: Boolean;
  
  constructor(
    private formBuilder: FormBuilder, 
    // private dialogRef: MatDialogRef<SearchPersonComponent>,
  ) { }

  ngOnInit() {
      this.form = this.formBuilder.group({
      nic : ['',Validators.required],
      appointmentletter: ['', Validators.required],
    });
  }

  getModel() {
    return this.form.value;
  }

  // close() {
  //   this.dialogRef.close();
  // }

  // saveAndClose() {
  //   if(this.form.valid) {
  //     this.dialogRef.close(this.form);
    
  //   } else {
  //     //this.alert.show('Please Fill the Form Correctly');
  //   }
  // }

  onSelectNic(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed 
        this.form.get('nic').setValue(event.target['result']) 
        console.log(event.target['result']);
      }
    }}

    onSelectAppointmentletter(event) { // called each time file input changes
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
  
        reader.readAsDataURL(event.target.files[0]); // read file as data url
  
        reader.onload = (event) => { // called once readAsDataURL is completed 
          this.form.get('appointmentletter').setValue(event.target['result']) 
          console.log(event.target['result']);
        }
      }}

}
