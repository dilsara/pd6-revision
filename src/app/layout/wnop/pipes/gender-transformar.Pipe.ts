import { Pipe, PipeTransform } from '@angular/core'; 

@Pipe({ name: 'GenderT' })
export class GenderTransfomarPipe implements PipeTransform {

    transform(value: number): string {
        return value == 0 ? 'Female' : 'Male'
    };

}