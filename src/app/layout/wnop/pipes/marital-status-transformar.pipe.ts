import { Pipe, PipeTransform } from '@angular/core'; 

@Pipe({ name: 'maritalStatusT' })
export class MaritalStatusTransfomarPipe implements PipeTransform {

    transform(value: number): string {
        if (value == 0) {
            return 'Unmarried'
        }
        else if(value == 1) {
            return 'Married'
        }
        else if (value ==2) {
            return 'Divorced/Separate/Widow'
        }
        else {
            return ''
        }
    };
}