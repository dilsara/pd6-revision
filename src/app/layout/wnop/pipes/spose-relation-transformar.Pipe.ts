import { Pipe, PipeTransform } from '@angular/core'; 

@Pipe({ name: 'SposeRelationT' })
export class SpouseRelationTransformarPipe implements PipeTransform {

    transform(value: number): string {
        return value == 0 ? 'Wife' : 'Husband'
    };

}
