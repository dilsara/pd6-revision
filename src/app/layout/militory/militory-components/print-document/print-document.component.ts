import { Component, OnInit, ViewChild } from '@angular/core';
import { PersonalInformationComponent } from '../personal-information/personal-information.component';
import { DependentInformationComponent } from '../dependent-information/dependent-information.component';
import { BankInformationComponent } from '../bank-information/bank-information.component';
import { PensionInformationComponent } from '../pension-information/pension-information.component';
import { ServiceInformationComponent } from '../service-information/service-information.component';
import { MainComponent } from '../main/main.component';

@Component({
  selector: 'app-print-document',
  templateUrl: './print-document.component.html',
  styleUrls: ['./print-document.component.scss']
})
export class PrintDocumentComponent implements OnInit {

  // @ViewChild(MainComponent) MainForm: MainComponent;
  // @ViewChild(ServiceInformationComponent) serviceInformationForm: ServiceInformationComponent;
  // @ViewChild(DependentInformationComponent) dependentInformationForm: DependentInformationComponent;
  // @ViewChild(BankInformationComponent) bankInformationForm: BankInformationComponent;
  // @ViewChild(PensionInformationComponent) pensionInformationForm: PensionInformationComponent;

  constructor() { }

  ngOnInit() {

  }

  print() {
    console.log("personalInforModel 1");
    // let personalInforModel = this.MainForm.getModel();
    console.log("personalInforModel 2");
  }
  printDoc() {
    let printContents, popupWin;
    printContents = document.getElementById('report_id').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();

    popupWin.document.write(`
      <html>
        <head>
          <title>Print Report</title>
          <style>
          
          </style>
          </head>
          <body onload="window.print();window.close()">${printContents}</body>
        </html>`
    );
    popupWin.document.close();
  }

}
