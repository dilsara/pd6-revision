import { Address } from "./address.model";

export class ServiceInformationModel {
    regementNo: string;
    rank: string;
    pensionableRank: string;
    grade: string;
    regement: string;
    officialAddress: Address
    soldierNo: string;
    servicePenNo: string;
    disabledPenNo: string;
    unit: string;
    appointmentDate: string;
    pensionOrDeathDate: string;
    pensionOrMedicalDate: string;
    wopNo: string;
    salaryCode: string;
    salaryCircular: string;
    service: string;
    designation: string;
}
