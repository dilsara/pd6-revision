export interface SpouseInfoTblModel {
    id : number;
    fullName: string;
    relation: boolean;
    // gender: string;
    dob: Date;
    nic: string; 
    birth_Cert_Number: number;
    marriage_Certificate_NO: number;
}