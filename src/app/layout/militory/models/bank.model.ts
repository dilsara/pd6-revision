export class BankModel{
    bank: string;
    branch: string;
    accountNo: string;
}