import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pd6RejectedComponent } from './pd6-rejected.component';

describe('Pd6RejectedComponent', () => {
  let component: Pd6RejectedComponent;
  let fixture: ComponentFixture<Pd6RejectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pd6RejectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pd6RejectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
