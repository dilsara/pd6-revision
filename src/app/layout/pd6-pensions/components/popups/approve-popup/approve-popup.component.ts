import { Component, OnInit, Inject } from '@angular/core';
import { PensionerService } from '../../../services/pensioner.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

    /**
     * subpaths
     * @author Shageesha Prabagaran
     */
@Component({
	selector: 'app-approve-popup',
	templateUrl: './approve-popup.component.html',
	styleUrls: ['./approve-popup.component.scss']
})
export class ApprovePopupComponent implements OnInit {
	data1: any;

	constructor(
		@Inject(MAT_DIALOG_DATA) public data,
		public pensionerservice: PensionerService,
		private notification: NotificationsService,
		private router: Router,
		public dialogRef: MatDialogRef<ApprovePopupComponent>) {
		dialogRef.disableClose = true;
	}

	ngOnInit() {
		console.log(this.data.status);
	}

	approve() {
		let user = localStorage.getItem("username");
		let penno = localStorage.getItem("pension_id");
		let refnumber = localStorage.getItem("ref");

		

		if(this.data.status == 100 || this.data.status == 200 ){
			var values = {
				id: parseInt(refnumber),
				status: 300,
				reson: "Approved Application",
				user: user
			}
		}else if(this.data.status == 101 || this.data.status == 201 ){
			var values = {
				id: parseInt(refnumber),
				status: 301,
				reson: "Approved Application",
				user: user
			}
		}

		this.pensionerservice.change_status(values).subscribe(data => {
			this.data1 = JSON.parse(JSON.stringify(data));
			if (this.data1.code == 200) {
				this.notification.openSnackBar("Application is approved", '', '');
				this.close();
			}
		})
		this.router.navigateByUrl("/login/pd6-pensions");
	}


	close(): void {
		this.dialogRef.close(); //close dialog
	}

}
