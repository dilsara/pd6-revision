import { Component, OnInit, Inject, Input } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { PensionerService } from '../../../services/pensioner.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ConfirmationPopupComponent } from '../confirmation-popup/confirmation-popup.component';
import { Pd6DetailedViewComponent } from '../../pd6-detailed-view/pd6-detailed-view.component';

@Component({
	selector: 'app-abovebpen-popup',
	templateUrl: './abovebpen-popup.component.html',
	styleUrls: ['./abovebpen-popup.component.scss']
})

/**
 * subpaths
 * @author Shageesha Prabagaran
 */

export class AbovebpenPopupComponent implements OnInit {

	reason: any;
	subRefArray = [];
	submisisonRef = null;
	submisisonCode = null;
	statusCode1: any;

	constructor(
		@Inject(MAT_DIALOG_DATA) public data,
		public dialogRef: MatDialogRef<AbovebpenPopupComponent>,
		private router: Router,
		private dialog: MatDialog,
		private pensionerservice: PensionerService,
		private notification: NotificationsService,
		private activatedRoutes: ActivatedRoute
	) {
		dialogRef.disableClose = true
	}

	ngOnInit() {
		console.log(this.data.status);
		this.activatedRoutes.params.subscribe((params: Params) => {
			this.statusCode1 = params['status']; // get route parameters

		})
	}

	close(): void {
		this.dialogRef.close();
	}

	submitReason() {

		if (this.pensionerservice.editstate == true) {
			let refnumber = localStorage.getItem("ref");
			let user = localStorage.getItem("username");
			let penno = localStorage.getItem("pension_id");

			this.pensionerservice.put_pensioner_details(parseInt(penno), this.pensionerservice.pensionmodel).subscribe(data => {
				let finaldata = JSON.parse(JSON.stringify(data));
				this.submisisonRef = finaldata.refNumber;
				localStorage.setItem("ref", this.submisisonRef);

				if (finaldata.code == 200) {
					if (this.data.status == 100 || this.data.status == 200 || this.data.status == 310) {
						var values = {
							id: parseInt(refnumber),
							status: 100,
							reson: "Application is edited",
							user: user
						}

						this.pensionerservice.change_status(values).subscribe(data => {
							let values1 = {
								revisionId: refnumber,
								reason: this.reason
							}

							if (this.reason = null || this.reason == "" || this.reason == undefined) {
								this.notification.openSnackBar("Provide a valid REASON, before proceeding !", '', '');
								this.reason = ""
							} else {
								this.pensionerservice.updateRevisionReasonAbove48(parseInt(refnumber), values1).subscribe(data => {
									let data1 = JSON.parse(JSON.stringify(data));
									this.close();
									this.confirmationPopup();
								})
							}
						});
					} else if (this.data.status == 101 || this.data.status == 201) {

						var values = {
							id: parseInt(refnumber),
							status: 101,
							reson: "Application is edited",
							user: user
						}

						this.pensionerservice.change_status(values).subscribe(data => {

							let values = {
								revisionId: refnumber,
								reason: this.reason
							}

							if (this.reason = null || this.reason == "" || this.reason == undefined) {
								this.notification.openSnackBar("Provide a valid REASON, before proceeding !", '', '');
								this.reason = ""
							} else {
								this.pensionerservice.updateRevisionReasonAbove48(parseInt(refnumber), values).subscribe(data => {
									let data1 = JSON.parse(JSON.stringify(data));
									this.close();
									this.confirmationPopup();
								})
							}
						});
					}
				} else {
					this.notification.openSnackBar("Submission Unsuccessful", '', '');
				}
			}, error => {
				if (this.pensionerservice.pensionmodel.personalInfo.gender == null ||
					this.pensionerservice.pensionmodel.pension_service.designation == null ||
					this.pensionerservice.pensionmodel.pension_service.service == " " ||
					this.pensionerservice.pensionmodel.personalInfo.title == " " ||
					this.pensionerservice.pensionmodel.personalInfo.dob == null) {
					this.notification.openSnackBar("Please Check the fields in the PERSONAL INFORMATION", '', '');
				} else if (error["error"]["message"] == "Check whether date of birth is correct") {
					this.notification.openSnackBar("Please enter Date of Birth", '', '');
				} else {
					this.notification.openSnackBar("Cannot Submit the application", '', '');
				}
			})

		} else {
			this.pensionerservice.sendpd6PensionDetails(this.pensionerservice.pensionmodel).subscribe(data => {
				this.subRefArray = [JSON.parse(JSON.stringify(data))];
				this.submisisonRef = this.subRefArray[0].refNumber;
				localStorage.setItem("ref", this.submisisonRef);
				this.submisisonCode = this.subRefArray[0].code;
				let refnumber = localStorage.getItem("ref");

				let values = {
					revisionId: refnumber,
					reason: this.reason
				}

				if (this.reason = null || this.reason == "" || this.reason == undefined) {
					this.notification.openSnackBar("Provide a valid REASON, before proceeding !", '', '');
					this.reason = ""
				} else {
					this.pensionerservice.postrevisionAboveReasons(values).subscribe(data => {
						let data1 = JSON.parse(JSON.stringify(data));
						this.close();
						this.confirmationPopup();
					})
				}

			}, error => {
				if (this.pensionerservice.pensionmodel.personalInfo.gender == null ||
					this.pensionerservice.pensionmodel.pension_service.designation == null ||
					this.pensionerservice.pensionmodel.pension_service.service == "" ||
					this.pensionerservice.pensionmodel.personalInfo.title == "" ||
					this.pensionerservice.pensionmodel.personalInfo.dob == "") {
					this.notification.openSnackBar("Please Check the fields in the PERSONAL INFORMATION", '', '');
				} else if (error["error"]["message"] == "Check whether date of birth is correct") {
					this.notification.openSnackBar("Please enter Date of Birth", '', '');
				} else {
					this.notification.openSnackBar("Cannot Submit the application", '', '');
				}
			})
		}


	}

	confirmationPopup() {
		const dialogRef = this.dialog.open(ConfirmationPopupComponent, {
			width: '650px',
			height: '300px',
			data: { refNumber: this.submisisonRef }
		});
		dialogRef.afterClosed().subscribe(result => {
		});
	}

}
