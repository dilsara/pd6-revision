import { Component, OnInit, ViewChild } from '@angular/core';
import { PensionerService } from '../../services/pensioner.service';
import { PensionModel } from '../../models/pension.model';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DataConfigService } from '../../dataconfig/data-config.service';
import { ViewPersonalInfoComponent } from "../pd6-detailed-view/view-personal-info/view-personal-info.component";
import { ViewPaymentInfoComponent } from "../pd6-detailed-view/view-payment-info/view-payment-info.component";
import { FillPersonalModel } from '../../models/fillpersonal.model';
import { SMSModel } from 'src/app/models/sms.model';
import { MatDialog } from '@angular/material';
import { ConfirmationPopupComponent } from '../popups/confirmation-popup/confirmation-popup.component';
import { SourceDocumentComponent } from '../pd6-detailed-view/source-document/source-document.component'
import { element } from 'protractor';
import { NotificationsService } from 'src/app/services/notifications.service';
import { error } from '@angular/compiler/src/util';
import { THIS_EXPR, STRING_TYPE } from '@angular/compiler/src/output/output_ast';
import { FillPaymentModel } from '../../models/fillpayment.model';
import { PensionerModel } from '../../models/pensioner.model';
import { AbovebpenPopupComponent } from '../popups/abovebpen-popup/abovebpen-popup.component';
import { ActivatedRoute, Params } from '@angular/router';

    /**
     * subpaths
     * @author Shageesha Prabagaran
     */
@Component({
	selector: 'app-pd6-detailed-view',
	templateUrl: './pd6-detailed-view.component.html',
	styleUrls: ['./pd6-detailed-view.component.scss']
})
export class Pd6DetailedViewComponent implements OnInit {

	@ViewChild(ViewPersonalInfoComponent) viewpersonal: ViewPersonalInfoComponent
	@ViewChild(ViewPaymentInfoComponent) viewpayment: ViewPaymentInfoComponent
	@ViewChild(SourceDocumentComponent) sourcedoc: SourceDocumentComponent

	firstFormGroup: FormGroup;
	secondFormGroup: FormGroup;
	thirdFormGroup: FormGroup;
	fourthFormGroup: FormGroup;
	pdsixdetails: FormControl;
	form: FormGroup;
	public pensionmodel: PensionModel;
	public smsmodel: SMSModel;
	public pensionermodel: PensionerModel;
	isOptional = false;
	subRefArray = [];
	submisisonRef = null;
	submisisonCode = null;
	allowDetails = []; statusCode1: any;

	constructor(private _formBuilder: FormBuilder,
		private dialog: MatDialog,
		private pensionerService: PensionerService,
		public dataConfig: DataConfigService,
		private notification: NotificationsService,
		private activatedRoutes: ActivatedRoute) {

		//Create an object for the pension model -- NESTED JSON
		this.pensionmodel = new PensionModel();
		this.smsmodel = new SMSModel();
	}

	ngOnInit() {
		this.firstFormGroup = this._formBuilder.group({
			firstCtrl: ['', Validators.required]
		});
		this.secondFormGroup = this._formBuilder.group({
			secondCtrl: ''
		});
		this.thirdFormGroup = this._formBuilder.group({
			secondCtrl: ''
		});
		this.fourthFormGroup = this._formBuilder.group({
			secondCtrl: ''
		});

		this.form = this._formBuilder.group({
			pdsixdetails: this._formBuilder.group({
			})
		});

		this.activatedRoutes.params.subscribe((params: Params) => {
			this.statusCode1 = params['status']; // get route parameters
		})
	}

	initFormChangeHooks() {
		console.log(this.form.get);
	}

	next() {
		if (this.viewpersonal.fillpersonal.gender == null) {
			this.notification.openSnackBar("Fill the gender", '', '');
		} else if (this.viewpersonal.fillpersonal.title == "") {
			this.notification.openSnackBar("Fill the Title", '', '');
		} else if (this.viewpersonal.fillpersonal.designation == null) {
			this.notification.openSnackBar("Fill the Designation", '', '');
		} else if (this.viewpersonal.fillpersonal.service == null) {
			this.notification.openSnackBar("Fill the Service", '', '');
		}
	}

	sendpd6details() {

		if (this.pensionerService.calculateState == false) {
			this.notification.openSnackBar("Please re-calculate", "", "");
			return;
		}

		//<----------------- PUT METHOD ---------------->
		if (this.pensionerService.editstate == true) {
			let refnumber = localStorage.getItem("ref");
			let user = localStorage.getItem("username");
			let penno = localStorage.getItem("pension_id");

			//Bind those details to the new pension model
			//PERSONAL INFROMATION ASSIGNED
			this.pensionmodel.personalInfo.title = this.viewpersonal.fillpersonal.title;
			this.pensionmodel.personalInfo.fullName = this.viewpersonal.pensiondetail.name;
			this.pensionmodel.personalInfo.gender = this.viewpersonal.fillpersonal.gender;
			this.pensionmodel.personalInfo.dob = this.viewpersonal.pensiondetail.dob;
			this.pensionmodel.personalInfo.address.addressLine1 = this.viewpersonal.pensiondetail.ad1;
			this.pensionmodel.personalInfo.address.addressLine2 = this.viewpersonal.pensiondetail.ad2;
			this.pensionmodel.personalInfo.address.addressLine3 = this.viewpersonal.pensiondetail.ad3;
			this.pensionmodel.personalInfo.nic = this.viewpersonal.pensiondetail.nic;
			this.pensionmodel.personalInfo.mobile = this.viewpersonal.mobprefix;
			this.pensionmodel.personalInfo.landNumber = this.viewpersonal.pensiondetail.phone;
			this.pensionmodel.personalInfo.gn = 0;
			this.pensionmodel.personalInfo.id = this.pensionerService.fillpersonalmodel.id;
			this.pensionmodel.personalInfo.dscode = this.viewpersonal.fillpersonal.dscode;

			//PENSION SERVICE ASSIGNED
			this.pensionmodel.pension_service.retired_date = this.viewpersonal.pensiondetail.rdate;
			this.pensionmodel.pension_service.retired_reason = "";
			this.pensionmodel.pension_service.section = this.viewpayment.fillpayment.section;
			this.pensionmodel.pension_service.nopay_days = this.viewpayment.fillpayment.nopayperiod_days;
			this.pensionmodel.pension_service.nopay_months = this.viewpayment.fillpayment.nopayperiod_months;
			this.pensionmodel.pension_service.nopay_years = this.viewpayment.fillpayment.nopayperiod_years;
			this.pensionmodel.pension_service.service_grade = this.viewpayment.fillpayment.service_grade;
			this.pensionmodel.pension_service.salary_scale = this.viewpayment.fillpayment.salary_scale;
			this.pensionmodel.pension_service.total_service_days = 0;
			this.pensionmodel.pension_service.total_service_months = 0;
			this.pensionmodel.pension_service.total_service_years = 0;
			this.pensionmodel.pension_service.net_service_days = this.viewpayment.fillpayment.servicperiod_days;
			this.pensionmodel.pension_service.net_service_months = this.viewpayment.fillpayment.servicperiod_months;
			this.pensionmodel.pension_service.net_service_years = this.viewpayment.fillpayment.servicperiod_years;
			this.pensionmodel.pension_service.designation = this.viewpersonal.fillpersonal.designation;
			this.pensionmodel.pension_service.service = this.viewpersonal.fillpersonal.service;

			//PENSION ASSIGNED
			this.pensionmodel.pension.id = penno;
			let x: any;
			x = this.pensionmodel.pension.id;
			localStorage.setItem("pension_id", x);
			this.pensionmodel.pension.pension_type = this.pensionerService.pt;
			this.pensionmodel.pension.circular = this.viewpayment.fillpayment.circular;
			this.pensionmodel.pension.increment_date = this.viewpayment.fillpayment.increment_date;
			this.pensionmodel.pension.net_reduced_percentage = this.viewpayment.rev_reduce_perc;
			this.pensionmodel.pension.net_unreduced_percentage = this.viewpayment.rev_unreduce_perc;
			this.pensionmodel.pension.basic_salary = this.viewpayment.basic_rev;
			this.pensionmodel.pension.reduced_salary = this.viewpayment.fixedrevreduced;
			this.pensionmodel.pension.unreduced_salary = this.viewpayment.fixedunrevreduced;
			this.pensionmodel.pension.total_amount_paid = this.viewpayment.totalpensiontobepaid;
			this.pensionmodel.pension.earned_increment_2015 = this.viewpayment.fillpayment.increment2015;
			this.pensionmodel.pension.earned_increment_2017 = this.viewpayment.fillpayment.increment2017;

			//REVISION - Contains 2015 and 2017 revised pension details
			this.pensionmodel.revision.id = parseInt(refnumber);
			this.pensionmodel.revision.pid = this.viewpersonal.pensiondetail.id;
			this.pensionmodel.revision.salary_scale = this.viewpayment.fillpayment.salary_scale;
			this.pensionmodel.revision.salary_increase = this.viewpayment.salaryincrease;

			//2017
			this.pensionmodel.revision.reduced_salary_revised = this.viewpayment.fixedrevreduced;
			this.pensionmodel.revision.unreduced_salary_revised = this.viewpayment.fixedunrevreduced;
			this.pensionmodel.revision.circular_revised = this.viewpayment.fillpayment.circular_revised;
			this.pensionmodel.revision.reduce_percentage_revised = this.viewpayment.rev_reduce_perc;
			this.pensionmodel.revision.unreduced_percentage_revised = this.viewpayment.rev_unreduce_perc;
			this.pensionmodel.revision.reduce_percentage_2020 = 0; //OMIT THIS 
			this.pensionmodel.revision.reduce_percentage_2020 = 0;
			//2015
			this.pensionmodel.revision.prev_salary_scale = this.viewpayment.fillpayment.salary_scale;
			this.pensionmodel.revision.prev_basic_salary = this.viewpayment.fillpayment.basic_salary;
			this.pensionmodel.revision.prev_circular = this.viewpayment.fillpayment.circular;
			this.pensionmodel.revision.prev_reduced_salary = this.viewpayment.fixreducedsal;
			this.pensionmodel.revision.prev_unreduced_salary = this.viewpayment.fixunreducedsal;
			this.pensionmodel.revision.prev_reduced_percentage = this.viewpayment.reduced_perc;
			this.pensionmodel.revision.prev_unreduced_percentage = this.viewpayment.unreduced_perc;

			this.viewpayment.allarray.forEach(element => {
				this.allowDetails.push(element);
			})

			this.viewpayment.nonallarray.forEach(element => {
				this.allowDetails.push(element);
			})
			this.pensionmodel.allowances.data = this.allowDetails;
			this.pensionerService.pensionmodel = this.pensionmodel;

			if (this.viewpayment.bpenVariance < this.pensionerService.bpenPension) {
				// this.notification.openSnackBar("Cannot submit the application - Total pension to be paid is considerably more than expected", '', '');
				this.abovePensionPopup();
			} else {
				this.pensionerService.put_pensioner_details(parseInt(penno), this.pensionmodel).subscribe(data => {
					let finaldata = JSON.parse(JSON.stringify(data));
					this.submisisonRef = finaldata.refNumber;
					localStorage.setItem("ref", this.submisisonRef);
					if (finaldata.code == 200) {
						if (this.statusCode1 == 100 || this.statusCode1 == 200 || this.statusCode1 == 310) {
							var values = {
								id: parseInt(refnumber),
								status: 100,
								reson: "Application is edited",
								user: user
							}
						} else if (this.statusCode1 == 101 || this.statusCode1 == 201) {
							var values = {
								id: parseInt(refnumber),
								status: 101,
								reson: "Application is edited",
								user: user
							}
						}
						this.pensionerService.change_status(values).subscribe(data => {
						});
						this.confirmationPopup();
						this.clearFields();

					} else {
						this.notification.openSnackBar("Submission Unsuccessful", '', '');
					}

				}, error => {
					if (this.pensionmodel.personalInfo.gender == null ||
						this.pensionmodel.pension_service.designation == null ||
						this.pensionmodel.pension_service.service == " " ||
						this.pensionmodel.personalInfo.title == " " ||
						this.pensionmodel.personalInfo.dob == null) {
						this.notification.openSnackBar("Please Check the fields in the PERSONAL INFORMATION", '', '');
					} else if (error["error"]["message"] == "Check whether date of birth is correct") {
						this.notification.openSnackBar("Please enter Date of Birth", '', '');
					} else {
						this.notification.openSnackBar("Cannot Submit the application", '', '');
					}
				})
			}
		}
		//<-----------POST METHOD------------->
		else if (this.pensionerService.editstate == false) {
			// console.log("POST METHOD");
			//PERSONAL INFROMATION ASSIGNED
			this.pensionmodel.personalInfo.title = this.viewpersonal.fillpersonal.title;
			this.pensionmodel.personalInfo.fullName = this.viewpersonal.pensiondetail.name;
			this.pensionmodel.personalInfo.gender = this.viewpersonal.fillpersonal.gender;
			this.pensionmodel.personalInfo.dob = this.viewpersonal.pensiondetail.dob;
			this.pensionmodel.personalInfo.address.addressLine1 = this.viewpersonal.pensiondetail.ad1;
			this.pensionmodel.personalInfo.address.addressLine2 = this.viewpersonal.pensiondetail.ad2;
			this.pensionmodel.personalInfo.address.addressLine3 = this.viewpersonal.pensiondetail.ad3;
			this.pensionmodel.personalInfo.nic = this.viewpersonal.pensiondetail.nic;
			this.pensionmodel.personalInfo.mobile = this.viewpersonal.mobprefix;
			this.pensionmodel.personalInfo.landNumber = this.viewpersonal.pensiondetail.phone;
			this.pensionmodel.personalInfo.gn = 0;
			this.pensionmodel.personalInfo.dscode = this.viewpersonal.fillpersonal.dscode;

			//PENSION SERVICE ASSIGNED
			this.pensionmodel.pension_service.retired_date = this.viewpersonal.pensiondetail.rdate;
			this.pensionmodel.pension_service.retired_reason = "";
			this.pensionmodel.pension_service.section = this.viewpayment.fillpayment.section;
			this.pensionmodel.pension_service.nopay_days = this.viewpayment.fillpayment.nopayperiod_days;
			this.pensionmodel.pension_service.nopay_months = this.viewpayment.fillpayment.nopayperiod_months;
			this.pensionmodel.pension_service.nopay_years = this.viewpayment.fillpayment.nopayperiod_years;
			this.pensionmodel.pension_service.service_grade = this.viewpayment.fillpayment.service_grade;
			this.pensionmodel.pension_service.salary_scale = this.viewpayment.fillpayment.salary_scale;
			this.pensionmodel.pension_service.total_service_days = 0;
			this.pensionmodel.pension_service.total_service_months = 0;
			this.pensionmodel.pension_service.total_service_years = 0;
			this.pensionmodel.pension_service.net_service_days = this.viewpayment.fillpayment.servicperiod_days;
			this.pensionmodel.pension_service.net_service_months = this.viewpayment.fillpayment.servicperiod_months;
			this.pensionmodel.pension_service.net_service_years = this.viewpayment.fillpayment.servicperiod_years;
			this.pensionmodel.pension_service.designation = this.viewpersonal.fillpersonal.designation;
			this.pensionmodel.pension_service.service = this.viewpersonal.fillpersonal.service;

			//PENSION ASSIGNED
			this.pensionmodel.pension.id = this.viewpersonal.pensiondetail.penno;
			localStorage.setItem("pension_id", this.pensionmodel.pension.id.toString());
			this.pensionmodel.pension.pension_type = this.viewpersonal.pensiondetail.pt;
			this.pensionmodel.pension.circular = this.viewpayment.fillpayment.circular;
			this.pensionmodel.pension.increment_date = this.viewpersonal.pensiondetail.rdate;
			this.pensionmodel.pension.net_reduced_percentage = this.viewpayment.rev_reduce_perc;
			this.pensionmodel.pension.net_unreduced_percentage = this.viewpayment.rev_unreduce_perc;
			this.pensionmodel.pension.basic_salary = this.viewpayment.basic_rev;
			this.pensionmodel.pension.reduced_salary = this.viewpayment.fixedrevreduced;
			this.pensionmodel.pension.unreduced_salary = this.viewpayment.fixedunrevreduced;
			this.pensionmodel.pension.total_amount_paid = this.viewpayment.totalpensiontobepaid;
			this.pensionmodel.pension.earned_increment_2015 = this.viewpayment.fillpayment.increment2015;
			this.pensionmodel.pension.earned_increment_2017 = this.viewpayment.fillpayment.increment2017;
			//REVISION - Contains 2015 and 2017 revised pension details
			this.pensionmodel.revision.pid = this.viewpersonal.pensiondetail.id;
			this.pensionmodel.revision.salary_scale = this.viewpayment.fillpayment.salary_scale;
			this.pensionmodel.revision.salary_increase = this.viewpayment.salaryincrease;

			//2017
			this.pensionmodel.revision.reduced_salary_revised = this.viewpayment.fixedrevreduced;
			this.pensionmodel.revision.unreduced_salary_revised = this.viewpayment.fixedunrevreduced;
			this.pensionmodel.revision.circular_revised = this.viewpayment.fillpayment.circular_revised;
			this.pensionmodel.revision.reduce_percentage_revised = this.viewpayment.rev_reduce_perc;
			this.pensionmodel.revision.unreduced_percentage_revised = this.viewpayment.rev_unreduce_perc;
			this.pensionmodel.revision.reduce_percentage_2020 = 0; //OMIT THIS 
			this.pensionmodel.revision.reduce_percentage_2020 = 0;
			//2015
			this.pensionmodel.revision.prev_salary_scale = this.viewpayment.fillpayment.salary_scale;
			this.pensionmodel.revision.prev_basic_salary = this.viewpayment.fillpayment.basic_salary;
			this.pensionmodel.revision.prev_circular = this.viewpayment.fillpayment.circular;
			this.pensionmodel.revision.prev_reduced_salary = this.viewpayment.fixreducedsal;
			this.pensionmodel.revision.prev_unreduced_salary = this.viewpayment.fixunreducedsal;
			this.pensionmodel.revision.prev_reduced_percentage = this.viewpayment.reduced_perc;
			this.pensionmodel.revision.prev_unreduced_percentage = this.viewpayment.unreduced_perc;

			this.viewpayment.allarray.forEach(element => {
				this.allowDetails.push(element);
			})

			this.viewpayment.nonallarray.forEach(element => {
				this.allowDetails.push(element);
			})

			this.pensionmodel.allowances.data = this.allowDetails;

			this.pensionerService.pensionmodel = this.pensionmodel;
			if (this.viewpayment.bpenVariance < this.pensionerService.bpenPension) {
				// this.notification.openSnackBar("Cannot submit the application - Total pension to be paid is considerably more than expected", '', '');
				this.abovePensionPopup();
			} else {
				//POST METHOD
				this.pensionerService.sendpd6PensionDetails(this.pensionmodel).subscribe(data => {
					this.subRefArray = [JSON.parse(JSON.stringify(data))];
					this.submisisonRef = this.subRefArray[0].refNumber;
					localStorage.setItem("ref", this.submisisonRef);
					this.submisisonCode = this.subRefArray[0].code;

					if (this.submisisonCode == 200) {
						//SMS SERVICE
						this.smsmodel.message = "Dear " + this.pensionmodel.personalInfo.fullName + ",  your record is saved successfuly.";
						this.smsmodel.to = this.pensionmodel.personalInfo.mobile;
						this.pensionerService.sendSms(this.smsmodel).subscribe(data => {
						})
						this.confirmationPopup();
						this.clearFields();

					} else {
						this.notification.openSnackBar("Submission Unsuccessful", '', '');
					}
				}, error => {

					if (this.pensionmodel.personalInfo.gender == null ||
						this.pensionmodel.pension_service.designation == null ||
						this.pensionmodel.pension_service.service == "" ||
						this.pensionmodel.personalInfo.title == "" ||
						this.pensionmodel.personalInfo.dob == "") {
						this.notification.openSnackBar("Please Check the fields in the PERSONAL INFORMATION", '', '');
					} else if (error["error"]["message"] == "Check whether date of birth is correct") {
						this.notification.openSnackBar("Please enter Date of Birth", '', '');
					} else {
						this.notification.openSnackBar("Cannot Submit the application", '', '');
					}
				})

			}

		}
	}

	confirmationPopup() {
		const dialogRef = this.dialog.open(ConfirmationPopupComponent, {
			width: '650px',
			height: '300px',
			data: { refNumber: this.submisisonRef }
		});
		dialogRef.afterClosed().subscribe(result => {
		});
	}

	abovePensionPopup() {
		const dialogRef = this.dialog.open(AbovebpenPopupComponent, {
			width: '650px',
			height: '350px',
			data: { refNumber: this.submisisonRef,status:this.statusCode1 }
		});
		dialogRef.afterClosed().subscribe(result => {
		});
	}

	clearFields() {
		this.pensionmodel = new PensionModel();
		this.viewpayment.fillpayment = new FillPaymentModel();
		this.pensionerService.pensionermodel = new PensionerModel();
		this.viewpersonal.fillpersonal = new FillPersonalModel();
		this.viewpersonal.pensiondetail = "";
		this.viewpayment.pensiondetail = new PensionerModel();
		this.viewpersonal.mobprefix = "";
		this.viewpayment.reduced_perc = "";
		this.viewpayment.unreduced_perc = "";
		this.viewpayment.fillpayment.increment2017 = null;
		this.viewpayment.fixreducedsal = "";
		this.viewpayment.fixunreducedsal = "";
		this.viewpayment.basic_rev = "";
		this.viewpayment.rev_reduce_perc = "";
		this.viewpayment.rev_unreduce_perc = "";
		this.viewpayment.fillpayment.increment2015 = null;
		this.viewpayment.fixedrevreduced = "";
		this.viewpayment.fixedunrevreduced = "";
		this.viewpayment.totalpensiontobepaid = "";
		this.viewpayment.salaryincrease = "";
	}

}
