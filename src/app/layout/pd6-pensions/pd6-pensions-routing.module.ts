import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchNicComponent } from './components/search-nic/search-nic.component';
import { Pd6DetailedViewComponent } from './components/pd6-detailed-view/pd6-detailed-view.component';
import { Pd6DashboardComponent } from './components/pd6-dashboard/pd6-dashboard.component';
import { SourceDocumentComponent } from './components/pd6-detailed-view/source-document/source-document.component';
import { Pd6PendingComponent } from './components/pd6-dashboard/pd6-pending/pd6-pending.component';
import { Pd6RejectedComponent } from './components/pd6-dashboard/pd6-rejected/pd6-rejected.component';
import { Pd6ApprovedComponent } from './components/pd6-dashboard/pd6-approved/pd6-approved.component';
import { Pd6AwardComponent } from './components/pd6-dashboard/pd6-award/pd6-award.component';
import { ReportComponent } from './components/report/report.component';
import { Pd6PaidComponent } from './components/pd6-dashboard/pd6-paid/pd6-paid.component';
import { Pd6NegativeComponent } from './components/pd6-dashboard/pd6-negative/pd6-negative.component';

    /**
     * subpaths
     * @author Shageesha Prabagaran
     */
const routes: Routes = [
  { path: '', component: Pd6DashboardComponent },
  { path: 'search-nic', component: SearchNicComponent },
  { path: 'pd6-detailed-view/:status', component: Pd6DetailedViewComponent },
  { path: 'pd6-sourcedoc/:status', component: SourceDocumentComponent },
  { path: 'pending', component: Pd6PendingComponent },
  { path: 'rejected', component: Pd6RejectedComponent },
  { path: 'approved', component: Pd6ApprovedComponent },
  { path: 'award/:status', component: Pd6AwardComponent },
  { path: 'report', component: ReportComponent },
  { path: 'payment', component: Pd6PaidComponent },
  { path: 'negative', component: Pd6NegativeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Pd6PensionsRoutingModule { }
