import { Injectable } from '@angular/core';
import { API } from 'src/app/http/api';
import { ConfigService } from 'src/app/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class Pd3GratuityService {

  constructor(
    private api: API,
    private configService: ConfigService
  ) { }

  // gratuity & bank details
  getGratuityBankDetails(refNo: number, gratuityId: string) {
    return this.configService.getWithToken(this.api.GET_GRATUITY_BANK_DETAILS(refNo, gratuityId));
  }

  // get all gratuities
  getAllGratuities(refNo: number) {
    return this.configService.getWithToken(this.api.GET_ALL_GRATUITIES(refNo))
  }

  // list pending gratuities
  getPendingGratuities(state: number) {
    return this.configService.getWithToken(this.api.GET_PENDING_GRATUITIES(state));
  }

  acceptGratuities(pensionerNo: number, gratuityId: number, state: number) {
    return this.configService.postWithToken(this.api.ACCEPT_GRATUITIES(pensionerNo, gratuityId, state), null);
  }

  //get gratuity bank details
  getGratuityBank(branchId: string) {
    return this.configService.getWithToken(this.api.GET_BRANCH_DETAILS("122", branchId));
  }

}
