import { Component, OnInit } from '@angular/core';
import { DataConfigService } from '../services/data-config.service';
import { Pd3PensionsService } from '../services/pd3-pensions.service';
import { MatDialog } from '@angular/material';
import { ExtraPaymentDetailsPopupComponent } from '../popups/extra-payment-details-popup/extra-payment-details-popup.component';
import { ServiceDetailsPopupComponent } from '../popups/service-details-popup/service-details-popup.component';
import { GratuityBankDetailsPopupComponent } from '../popups/gratuity-bank-details-popup/gratuity-bank-details-popup.component';
import { PD3ButtonConfigModel } from 'src/app/models/pd3-button-config.model';
import { ActivatedRoute, Params } from '@angular/router';
import { StateChangeConfirmationPopupComponent } from '../popups/state-change-confirmation-popup/state-change-confirmation-popup.component';
import { ExtraPaymentDetails } from '../models/extra-paymets.model';
import { Observable } from 'rxjs';
import { PrintSathkaraPopupComponent } from '../popups/print-sathkara-popup/print-sathkara-popup.component';
import { InterviewDetailsModel } from '../models/interview-details.model';

@Component({
  selector: 'app-pd3-pension-information',
  templateUrl: './pd3-pension-information.component.html',
  styleUrls: ['./pd3-pension-information.component.scss']
})
export class Pd3PensionInformationComponent implements OnInit {


  consolidatedSalary2020: number;
  totalAllowance: number;

  reducedSalary: number;
  reducedSalary2020: number;
  unreducedSalary: number;
  unreducedSalary2020: number;

  total: number;
  total2020: number;

  public buttonConfig: PD3ButtonConfigModel;
  private interview: InterviewDetailsModel;
  public state: string;
  private refNumber: number;
  private pensionNumber: number;

  constructor(
    public dataConfig: DataConfigService,
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private pensionService: Pd3PensionsService
  ) {
    this.buttonConfig = new PD3ButtonConfigModel();
    this.interview = new InterviewDetailsModel();
    this.totalAllowance = 0.00;
    this.reducedSalary = 0.00;
    this.unreducedSalary = 0.00;
    this.reducedSalary2020 = 0.00;
    this.unreducedSalary2020 = 0.00;
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.state = params["state"];
      this.refNumber = params["refNumber"];
      this.pensionNumber = params["pensionNumber"];
      this.buttonConfig.getButtonConfiguration(localStorage.getItem("userRole"), this.state);
      this.pensionService.getInterviewDetails(this.refNumber)
        .subscribe(data => {
          this.interview = JSON.parse(JSON.stringify(data));
          console.log(data);
        })
    })

  }



  //popup view for extra payment details
  extraDetailsPopup(): void {
    const dialogRef = this.dialog.open(ExtraPaymentDetailsPopupComponent, {
      width: '650px',
      height: '550px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // popup for service details
  serviceDetailsPopup(): void {
    const dialogRef = this.dialog.open(ServiceDetailsPopupComponent, {
      width: '650px',
      height: '550px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // popup for gratuity details
  gratuityDetailsPopup(): void {
    const dialogRef = this.dialog.open(GratuityBankDetailsPopupComponent, {
      width: '650px',
      height: '550px',
      data: {
        gratuitybranch: this.dataConfig.pd3GratuityBankDetails.branch.url,
        pensionBranch: this.dataConfig.pd3Pension.branch.url,
        accountNo: this.dataConfig.pd3Pension.accountNumber
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // popup for gratuity details
  stateChangeConfirmationPopup(state: string): void {
    if (localStorage.getItem("userRole") == "SATHKARA_OFFICER") {
      const dialogRef = this.dialog.open(PrintSathkaraPopupComponent, {
        width: '650px',
        height: '550px',
        data: { pensionNumber: this.pensionNumber, interviewDate: this.interview.interviewDate }
      });

      dialogRef.afterClosed().subscribe(result => {
      });
    } else {
      const dialogRef = this.dialog.open(StateChangeConfirmationPopupComponent, {
        width: '430px',
        height: '170px',
        data: { state: state, referenceNumber: this.refNumber, pensionNo: this.pensionNumber, applicationState: this.state }
      });

      dialogRef.afterClosed().subscribe(result => {
      });
    }

  }

  // popup for service details
  printSathkaraPopup(): void {

  }




}
