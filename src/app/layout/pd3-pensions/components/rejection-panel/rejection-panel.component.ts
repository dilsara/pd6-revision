import { Component, OnInit, Input } from '@angular/core';
import { Pd3PensionsService } from '../../services/pd3-pensions.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { DataConfigService } from '../../services/data-config.service';
import { RejectionModel } from '../../models/rejection.model';
import { NotificationsService } from 'src/app/services/notifications.service';
import { error } from 'util';

@Component({
  selector: 'app-rejection-panel',
  templateUrl: './rejection-panel.component.html',
  styleUrls: ['./rejection-panel.component.scss']
})
export class RejectionPanelComponent implements OnInit {

  rejectReasons = [];
  selectedRow1;
  selectedRow2;
  leftTabRowNo: number;
  rightTabRowNo: number;
  reasonTab = [];
  reasonIdList = [];
  reasonTabList = [];
  reasonList = [];
  newReasonId: Number;
  checked: Boolean = false;
  allReasonList: any;
  refNumber: number;
  customReason: string;
  rejectionInstance: RejectionModel;
  state: string;
  pensionNumber: number;

  @Input() inputValues = {
    message: ""
  }

  constructor(
    private pensionService: Pd3PensionsService,
    private activatedRoute: ActivatedRoute,
    public dataConfig: DataConfigService,
    private notify: NotificationsService,
    private router: Router
  ) {
    this.rejectionInstance = new RejectionModel();
    this.state = "";
    this.pensionNumber = 0;
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.refNumber = params["refNumber"];
      this.state = params["state"];
      this.pensionNumber = params["pensionNumber"];
    })
    this.getAllPd3RejectionReasons();
  }

  // retrieve pd3 rejection reasons list

  getAllPd3RejectionReasons() {
    this.pensionService.getpd3RejectReasons()
      .subscribe(data => {
        this.rejectReasons = data["reasons"];
        this.reasonList = this.rejectReasons;
      })
  }

  //for remove reason method
  removeReason() {
    this.reasonTab.splice(this.rightTabRowNo, 1);
    this.reasonIdList.splice(this.rightTabRowNo, 1);
    this.selectedRow2 = null;

  }

  searchReason() {
    this.reasonList = this.rejectReasons.filter(item => item.message.toLowerCase().startsWith(this.inputValues.message.toLowerCase()));
  }


  //set clicked row data
  setClickedRow(index) {
    this.selectedRow1 = index;
    this.leftTabRowNo = index;
  }

  // remove selected row data
  deleteClickedRow(index) {
    this.selectedRow2 = index;
    this.rightTabRowNo = index;
  }

  //for addReason method
  addReason() {
    this.reasonTab.push(this.reasonList[this.leftTabRowNo].message);
    this.reasonIdList.push(this.reasonList[this.leftTabRowNo].id);
    this.leftTabRowNo = null;
    this.getAllPd3RejectionReasons();
    this.selectedRow1 = null;
  }

  //confirm rejection
  confirmReason() {
    this.rejectionInstance.rejectedBy = localStorage.getItem("username");
    var today = new Date();
    this.rejectionInstance.rejectedOn = today.toISOString();
    this.rejectionInstance.reasons = this.reasonTab;
    this.rejectionInstance.pension.id = this.refNumber;
    this.rejectionInstance.pension.name = this.dataConfig.pd3BasicInformation.name;
    this.pensionService.rejectApplication(this.refNumber, this.pensionNumber) // state update call
      .subscribe(() => {
        this.notify.openSnackBar("Pension application rejected successfully", "", 'green');
        this.router.navigateByUrl("/pd3-pensions/pd3-list-view/" + this.state);
      })
    // this.pensionService.confirmRejection(this.rejectionInstance)
    //   .subscribe(() => {
    //   }, error => {
    //     this.notify.openSnackBar("Something went wrong, rejection unsuccessfull", "", 'green');
    //   })
  }

  addCustomReason() {
    this.reasonTab.push(this.customReason);
    this.customReason = "";
  }

  clear() {
    this.reasonTab = [];
    this.customReason = "";
  }

}
