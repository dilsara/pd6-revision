import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPensionsComponent } from './edit-pensions.component';

describe('EditPensionsComponent', () => {
  let component: EditPensionsComponent;
  let fixture: ComponentFixture<EditPensionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPensionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPensionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
