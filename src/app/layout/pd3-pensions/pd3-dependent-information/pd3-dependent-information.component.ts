import { Component, OnInit, Input } from '@angular/core';
import { Pd3PensionsService } from '../services/pd3-pensions.service';
import { DataConfigService } from '../services/data-config.service';
import { ActivatedRoute, Params } from '@angular/router';
import { MatDialog } from '@angular/material';
import { DependentInformationPopupComponent } from '../popups/dependent-information-popup/dependent-information-popup.component';
import { error } from 'util';
import { PD3DependentModel } from '../models/pd3-dependent.model';

@Component({
  selector: 'app-pd3-dependent-information',
  templateUrl: './pd3-dependent-information.component.html',
  styleUrls: ['./pd3-dependent-information.component.scss']
})
export class Pd3DependentInformationComponent implements OnInit {

  refNumber: number;

  constructor(
    private pensioneService: Pd3PensionsService,
    public dataConfig: DataConfigService,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.refNumber = params['refNumber'];
      this.pensioneService.getPd3PensionerDependents(this.refNumber)
        .subscribe(data => {
          this.dataConfig.pd3DependentArray = data['dependents'];
        }, error => {
          this.dataConfig.pd3DependentArray = new Array<PD3DependentModel>();
        })
    })
  }

  //popup view for view pension history
  dependentInformationPopup(dependentId: number): void {
    const dialogRef = this.dialog.open(DependentInformationPopupComponent, {
      width: '650px',
      height: '600px',
      data: { dependentId: dependentId, refnumber: this.refNumber }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
