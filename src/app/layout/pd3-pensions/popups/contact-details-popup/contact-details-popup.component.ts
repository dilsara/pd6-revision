import { Component, OnInit } from '@angular/core';
import { DataConfigService } from '../../services/data-config.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-contact-details-popup',
  templateUrl: './contact-details-popup.component.html',
  styleUrls: ['./contact-details-popup.component.scss']
})
export class ContactDetailsPopupComponent implements OnInit {

  constructor(
    public dataConfig: DataConfigService,
    private dialogRef: MatDialogRef<ContactDetailsPopupComponent>
  ) { }

  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close(); //close dialog
  }

}
