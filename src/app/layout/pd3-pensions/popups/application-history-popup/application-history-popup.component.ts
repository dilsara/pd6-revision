import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ReferenceNumber } from '../../view-pensions/view-pensions.component';
import { Pd3PensionsService } from '../../services/pd3-pensions.service';
import { DataConfigService } from '../../services/data-config.service';
import { error } from 'util';
// import { ReceiveApplicationPopupComponent } from '../receive-application-popup/receive-application-popup.component';

@Component({
  selector: 'app-application-history-popup',
  templateUrl: './application-history-popup.component.html',
  styleUrls: ['./application-history-popup.component.scss']
})
export class ApplicationHistoryPopupComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ApplicationHistoryPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ReferenceNumber,
    private pensionsService: Pd3PensionsService,
    public dataConfig: DataConfigService
  ) { }

  ngOnInit() {
    this.pd3ViewPensionHistory(this.data.refNo); //pension history service call
  }

  close(): void {
    this.dialogRef.close(); //close dialog
  }

  // pension history 
  pd3ViewPensionHistory(pensionNo: number) {
    this.pensionsService.getPd3PensionHistory(111, pensionNo)
      .subscribe(data => {
        this.dataConfig.pd3PensionHistory = data["history"];
      }, error => {

      })
  }


}
