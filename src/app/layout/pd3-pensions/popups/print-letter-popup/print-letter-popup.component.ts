import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Pd3PensionsService } from '../../services/pd3-pensions.service';
import { InterviewModel } from '../../models/interview.model';
import { NotificationsService } from 'src/app/services/notifications.service';
import { error } from 'util';
import { SMSModel } from 'src/app/models/sms.model';

@Component({
  selector: 'app-print-letter-popup',
  templateUrl: './print-letter-popup.component.html',
  styleUrls: ['./print-letter-popup.component.scss']
})
export class PrintLetterPopupComponent implements OnInit {

  newInterview: InterviewModel;
  time: boolean;
  interviewTimes: string[];
  interviewSms: SMSModel;
  selectedPensioners = [];
  selectedTime: string;

  constructor(
    public dialogRef: MatDialogRef<PrintLetterPopupComponent>,
    private pensionService: Pd3PensionsService,
    private notify: NotificationsService,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.newInterview = new InterviewModel();
    this.interviewSms = new SMSModel();
    this.time = false;
    this.interviewTimes = ["09.00", "09.30", "10.00", "10.30", "11.00", "11.30", "12.00", "12.30", "13.00", "13.30", "14.00",
      "14.30", "15.00"];
  }

  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close(); //close dialog
  }

  confirm() {
    this.data.pensioners.forEach(refNo => {
      this.pensionService.getPensions(refNo)
        .subscribe(data => {
          data["pensions"].forEach(element => {
            this.newInterview.id = refNo;
            this.pensionService.printLetter(refNo, this.newInterview)
              .subscribe(data => {
                this.pensionService.getPd3PensionersDetails(refNo)
                  .subscribe(data => {
                    this.interviewSms.message = "Dear " + data["salutation"] + "." + data["name"] + ",  You are hereby invited for an interview at DoP on "
                      + this.newInterview.interviewDate + " at " + this.selectedTime + ".";
                    this.interviewSms.to = data["mobileNumber"];
                    this.pensionService.sendInterviewSms(this.interviewSms)
                      .subscribe(() => {
                        this.pensionService.acceptApplication(refNo, element.id)
                          .subscribe(() => {
                            this.notify.openSnackBar('Applications accepted successfully', '', '');
                          })
                      }, error => {
                        this.notify.openSnackBar('Error with sending sms', '', '');
                      })
                  }, error => {
                    this.notify.openSnackBar('Error while retrieving pensioner details', '', '');
                  })
                let subscription = this.pensionService.sendInterviewSms(this.interviewSms)
                  .subscribe(data => {
                    this.dialogRef.close();
                  }, error => {
                    this.notify.openSnackBar('Error with state update', '', '');
                  })
                setTimeout(() => subscription.unsubscribe(), 10000);
              }, error => {
                this.notify.openSnackBar('Error with printing letter', '', '');
              })

          });
        }, error => {
          this.notify.openSnackBar('Error while retrieving', '', '');
        })
    });

    // this.notify.openSnackBar('Application accepted successfully', '', '');
    // this.pensionService.printLetter()
  }

  // confirm1() {
  //   this.data.pensioners.forEach(refNo => {
  //     this.pensionService.getPensions(refNo)
  //       .subscribe(data => {
  //         data["pensions"].forEach(element => {
  //           this.newInterview.id = refNo;
  //           this.pensionService.printLetter(refNo, this.newInterview)
  //             .subscribe(data => {
  //               let subscription = this.pensionService.acceptApplication(refNo, element.id)
  //                 .subscribe(data => {
  //                   this.dialogRef.close();
  //                   this.pensionService.getPd3PensionersDetails(refNo)
  //                     .subscribe(data => {
  //                       this.interviewSms.message = "Dear " + data["salutation"] + "." + data["name"] + ",  You are hereby invited for an interview at DoP on "
  //                         + this.newInterview.interviewDate + " at " + this.selectedTime + ".";
  //                       this.interviewSms.to = data["mobileNumber"];
  //                       this.pensionService.sendInterviewSms(this.interviewSms)
  //                         .subscribe(() => {
  //                           this.notify.openSnackBar('Applications accepted successfully', '', '');
  //                         }, error => {
  //                           this.notify.openSnackBar('Error with sending sms', '', '');
  //                         })
  //                     }, error => {
  //                       this.notify.openSnackBar('Error while retrieving pensioner details', '', '');
  //                     })
  //                 }, error => {
  //                   this.notify.openSnackBar('Error with state update', '', '');
  //                 })
  //               setTimeout(() => subscription.unsubscribe(), 10000);
  //             }, error => {
  //               this.notify.openSnackBar('Error with printing letter', '', '');
  //             })

  //         });
  //       }, error => {
  //         this.notify.openSnackBar('Error while retrieving', '', '');
  //       })
  //   });

  // }
}
