import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportMainViewComponent } from './report-main-view.component';

describe('ReportMainViewComponent', () => {
  let component: ReportMainViewComponent;
  let fixture: ComponentFixture<ReportMainViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportMainViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportMainViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
