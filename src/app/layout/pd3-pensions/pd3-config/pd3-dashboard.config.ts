export default class PD3DashboardConfig {

    dashboardConfig: Object[];

    constructor() {

    }

    getDashboardConfig(userRole: string) {
        if (userRole == "POSTAL") {
            this.dashboardConfig = [
                {
                    "name": "Pending",
                    "path": "pd3-list-view/pending",
                    "color": "#8e44ad"
                },
                {
                    "name": "Rejected",
                    "path": "pd3-list-view/rejected",
                    "color": "#e74c3c"
                },
                {
                    "name": "Corrected",
                    "path": "pd3-list-view/corrected",
                    "color": "#16a085"
                }
            ]
        }
        else if (userRole == "DATA_ENTRY_OFFICER") {
            this.dashboardConfig = [
                {
                    "name": "Pending",
                    "path": "pd3-list-view/pending",
                    "color": "#8e44ad"
                },
                {
                    "name": "Rejected",
                    "path": "pd3-list-view/rejected",
                    "color": "#e74c3c"
                },
                {
                    "name": "Corrected",
                    "path": "pd3-list-view/corrected",
                    "color": "#16a085"
                }
            ]
        }
        else if (userRole == "SATHKARA_OFFICER") {
            this.dashboardConfig = [
                {
                    "name": "Pending",
                    "path": "pd3-list-view/pending",
                    "color": "#8e44ad"
                },
                {
                    "name": "Rejected",
                    "path": "pd3-list-view/rejected",
                    "color": "#e74c3c"
                },
                {
                    "name": "Corrected",
                    "path": "pd3-list-view/corrected",
                    "color": "#16a085"
                }
            ]
        }
        else if (userRole == "PRINT_LETTER") {
            this.dashboardConfig = [
                {
                    "name": "Pending",
                    "path": "pd3-list-view/pending",
                    "color": "#8e44ad"
                },
                {
                    "name": "Rejected",
                    "path": "pd3-list-view/rejected",
                    "color": "#e74c3c"
                },
                {
                    "name": "Corrected",
                    "path": "pd3-list-view/corrected",
                    "color": "#16a085"
                }
            ]
        }
        else if (userRole == "REGISTRATION_AD") {
            this.dashboardConfig = [
                {
                    "name": "Pending",
                    "path": "pd3-list-view/pending",
                    "color": "#8e44ad"
                },
                {
                    "name": "Rejected",
                    "path": "pd3-list-view/rejected",
                    "color": "#e74c3c"
                },
                {
                    "name": "Corrected",
                    "path": "pd3-list-view/corrected",
                    "color": "#16a085"
                }
            ]
        }
    }

}