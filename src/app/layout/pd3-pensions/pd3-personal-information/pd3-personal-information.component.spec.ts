import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pd3PersonalInformationComponent } from './pd3-personal-information.component';

describe('Pd3PersonalInformationComponent', () => {
  let component: Pd3PersonalInformationComponent;
  let fixture: ComponentFixture<Pd3PersonalInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pd3PersonalInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pd3PersonalInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
