export class PendingGratuityModel {
    id: number;
    pensioner: Pensioner;
}

class Pensioner {
    rel: string;
    url: string;
}