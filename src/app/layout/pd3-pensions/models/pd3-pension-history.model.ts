export class PD3PensionHistory {
    responsibleEmployee: string;
    state: string;
    timeStamp: string;
    name: string;
    nic: string;
    designation: string;

    constructor() {
        this.responsibleEmployee = "";
        this.state = "";
        this.timeStamp = "";
    }
}