export class RejectionModel {
    rejectedOn: string;
    rejectedBy: string;
    pension: Pension;
    reasons: Array<Reason>;

    constructor() {
        this.rejectedBy = "";
        this.rejectedOn = "";
        this.reasons = new Array<Reason>();
        this.pension = new Pension();
    }

}

class Pension {
    id: number;
    name: string;
}

class Reason {
    id: number;
    message: string;
}