import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material';
import { ReceiveApplicationPopupComponent } from '../popups/receive-application-popup/receive-application-popup.component';
import { ApplicationHistoryPopupComponent } from '../popups/application-history-popup/application-history-popup.component';
import { PD3ButtonConfigModel } from 'src/app/models/pd3-button-config.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ListViewService } from '../services/list-view.service';
import { DataConfigService } from '../services/data-config.service';
import { PD3ListView } from '../models/pd3-listview.model';
import { error } from 'util';
import { SpinnerService } from 'src/app/services/spinner.service';
import { Pd3ViewPensionsService } from '../services/pd3-view-pensions.service';
import { Pd3PensionsService } from '../services/pd3-pensions.service';
import { PrintLetterPopupComponent } from '../popups/print-letter-popup/print-letter-popup.component';

@Component({
  selector: 'app-pd3-list-view',
  templateUrl: './pd3-list-view.component.html',
  styleUrls: ['./pd3-list-view.component.scss']
})
export class Pd3ListViewComponent implements OnInit {

  displayedColumns: string[];
  dataSource = new MatTableDataSource<PD3ListView>();
  noRecordsState: boolean;
  selectedPensioners = [];
  noOfRecords: number;
  referenceNo: number;
  name: string;
  nic: string;
  penNo: number;
  kill: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  //called variables
  public state: string;
  public buttonConfig: PD3ButtonConfigModel;


  constructor(
    public dialog: MatDialog,
    private activatedRoutes: ActivatedRoute,
    private listViewService: ListViewService,
    public pd3DataConfig: DataConfigService,
    public listViewSpinner: SpinnerService,
    private pensions: Pd3ViewPensionsService,
    private router: Router,
    private pensionService: Pd3PensionsService,
    public dataConfig: DataConfigService

  ) {
    this.buttonConfig = new PD3ButtonConfigModel();
    this.noRecordsState = false;
    this.referenceNo = 0;
    this.name = "";
    this.nic = "";
    this.penNo = 0;
    this.kill = false;
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator; //page paginator
    this.activatedRoutes.params.subscribe((params: Params) => {
      this.state = params['state']; // get route parameters
      this.buttonConfig.getButtonConfiguration(localStorage.getItem("userRole"), this.state);
      this.getCustomisedTable(localStorage.getItem("userRole")); // configuration of table
    })
    if (localStorage.getItem("userRole") == "PRINT_LETTER") {
      this.getPd3ListView(this.state);
    }

  }

  //customise the mat table
  getCustomisedTable(role: string) {
    if (role == "POSTAL") {
      this.displayedColumns = ['refNumber', 'name', 'nic', 'retiredDate', 'designation', 'wopNumber', 'receive'];
    } else if (role == "DATA_ENTRY_OFFICER") {
      this.displayedColumns = ['refNumber', 'name', 'nic', 'retiredDate', 'designation', 'wopNumber', 'more'];
    } else if (role == "SATHKARA_OFFICER") {
      this.displayedColumns = ['refNumber', 'name', 'nic', 'retiredDate', 'designation', 'wopNumber', 'more'];
    } else if (role == "PRINT_LETTER") {
      this.displayedColumns = ['refNumber', 'name', 'nic', 'retiredDate', 'designation', 'wopNumber', 'select'];
    }
  }

  //pd3 list view
  getPd3ListView(state: string) {
    this.listViewSpinner.listViewSpinner = true;
    this.pd3DataConfig.pd3ListView = new Array<PD3ListView>();
    this.listViewService.getPD3ListView(state)
      .subscribe(data => { 
        this.pd3DataConfig.pd3ListView = data["pensioners"];
        // sorting out by retired date
        this.pd3DataConfig.pd3ListView.sort(function (a, b) {
          var aa = a.dateOfRetired.split('/').reverse().join(),
            bb = b.dateOfRetired.split('/').reverse().join();
          return aa < bb ? -1 : (aa > bb ? 1 : 0);
        });
        this.dataSource = new MatTableDataSource<PD3ListView>(this.pd3DataConfig.pd3ListView);
        setTimeout(() => this.dataSource.paginator = this.paginator);
        this.listViewSpinner.listViewSpinner = false;
      }, error => {
        this.listViewSpinner.listViewSpinner = false;
        this.noRecordsState = true;
      })
  }

  //filter by pension id
  applyFilterForId(filterValue: string) {
    this.dataSource.filterPredicate = (data:
      PD3ListView, filterValue: string) =>
      data.id.toString().toLowerCase().indexOf(filterValue) !== -1;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  //filter by nic
  applyFilterForNIC(filterValue: string) {
    this.dataSource.filterPredicate = (data:
      PD3ListView, filterValue: string) =>
      data.nic.toString().toLowerCase().indexOf(filterValue) != -1;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  //filter by name
  applyFilterForName(filterValue: string) {
    this.dataSource.filterPredicate = (data:
      PD3ListView, filterValue: string) =>
      data.name.toLowerCase().indexOf(filterValue) != -1;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  //pop up view for receive application
  receiveApplicationPopup(): void {
    const dialogRef = this.dialog.open(ReceiveApplicationPopupComponent, {
      width: '500px',
      height: '450px'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  getPD3ViewPensions(refNo: number) {
    this.listViewSpinner.viewPensionsSpinner = true;
    this.pensions.getPd3ViewPensions(refNo)
      .subscribe(data => {
        this.pd3DataConfig.pd3ViewPensions = data["pensions"];
        this.getPensionerDetails(refNo);
        this.router.navigateByUrl("/pd3-pensions/" + this.state + "/pd3-view-pensions/" + refNo);
        this.listViewSpinner.viewPensionsSpinner = false;
      }, error => {

      })
  }

  getPensionerDetails(refNo: number) {
    this.pensionService.getPd3PensionersDetails(refNo)
      .subscribe(data => {
        this.pd3DataConfig.pd3Pensioner = JSON.parse(JSON.stringify(data));
      }, error => {

      })
  }

  // popup for gratuity details
  printLetterPopup(): void {
    this.getCheckboxes();
    console.log(this.selectedPensioners);
    const dialogRef = this.dialog.open(PrintLetterPopupComponent, {
      width: '650px',
      height: '550px',
      data: { pensioners: this.selectedPensioners, count: this.selectedPensioners.length }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getPd3ListView(this.state);
    });
  }


  getCheckboxes() {
    this.selectedPensioners = (this.pd3DataConfig.pd3ListView).filter(x => x.checked === true).map(x => x.id);
    console.log(this.selectedPensioners);
  }

  customCheck() {
    for (let i = 0; i < this.noOfRecords; i++) {
      this.pd3DataConfig.pd3ListView[i].checked = true;
    }
  }

  clearCheck() {
    for (let i = 0; i < this.noOfRecords; i++) {
      this.pd3DataConfig.pd3ListView[i].checked = false;
    }
    this.noOfRecords = 0;
  }

  searchPensioner() {
    this.kill = true;
    this.listViewSpinner.listViewSpinner = true;
    this.pensionService.searchPensioner(this.referenceNo, this.name, this.nic, this.penNo)
      .subscribe(data => {
        this.pd3DataConfig.pd3ListView = data["pensioners"];
        this.dataSource = new MatTableDataSource<PD3ListView>(this.pd3DataConfig.pd3ListView);
        this.listViewSpinner.listViewSpinner = false;
        setTimeout(() => this.dataSource.paginator = this.paginator);
      })
  }

  refresh() {
    this.nic = "";
    this.penNo = 0;
    this.name = "";
    this.referenceNo = 0;

    // this.getPd3ListView(this.state);
  }

}



