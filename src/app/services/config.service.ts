import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Authentication } from '../models/auth.model';

@Injectable({
	providedIn: 'root'
})

    /**
     * subpaths
     * @author Shageesha Prabagaran
     */

export class ConfigService {

	constructor(
		private http: HttpClient
	) { }

	/**
	 * service callers 
	 */

	//http get
	get(url: string) {
		return this.http.get(url);
	}

	//http get with session id
	getWithToken(url: string) {
		const httpOptions = {
			headers: new HttpHeaders({
				'token': localStorage.getItem("token")
				// 'token': "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwIiwiaWF0IjoxNTY4ODY1NDMwLCJzdWIiOiJhdXRoX3Rva2VuIiwiaXNzIjoicGVuc2lvbmRwdCIsImIiOiJ0ZXN0cG8zIiwiYSI6IkRTX1BFTlNJT05fT0ZGSUNFUiIsImQiOjEsImV4cCI6MTU2ODg5NDIzMH0.CKYthhYxCfZ341ru-GrusBxFkiV6Csa7kdM_lBfQiX0"

			})
		};
		return this.http.get(url, httpOptions);
	}

	//http post
	post(url: string, data: any) {
		return this.http.post(url, data);
	}

	//http post with session id
	postWithToken(url: string, data: any) {
		const httpOptions = {
			headers: new HttpHeaders({
				'token': localStorage.getItem("token")
				// 'token': "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwIiwiaWF0IjoxNTY4ODY1NDMwLCJzdWIiOiJhdXRoX3Rva2VuIiwiaXNzIjoicGVuc2lvbmRwdCIsImIiOiJ0ZXN0cG8zIiwiYSI6IkRTX1BFTlNJT05fT0ZGSUNFUiIsImQiOjEsImV4cCI6MTU2ODg5NDIzMH0.CKYthhYxCfZ341ru-GrusBxFkiV6Csa7kdM_lBfQiX0"
			})
		};
		return this.http.post(url, data, httpOptions);
	}

	//http put
	put(url: string, data: any) {
		const httpOptions = {
			headers: new HttpHeaders({
				'token': localStorage.getItem("token")
				// 'token': "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwIiwiaWF0IjoxNTY4ODY1NDMwLCJzdWIiOiJhdXRoX3Rva2VuIiwiaXNzIjoicGVuc2lvbmRwdCIsImIiOiJ0ZXN0cG8zIiwiYSI6IkRTX1BFTlNJT05fT0ZGSUNFUiIsImQiOjEsImV4cCI6MTU2ODg5NDIzMH0.CKYthhYxCfZ341ru-GrusBxFkiV6Csa7kdM_lBfQiX0"
			})
		};
		return this.http.put(url, data, httpOptions);
	}

	//http delete
	delete(url: string) {
		return this.http.delete(url);
	}


}
