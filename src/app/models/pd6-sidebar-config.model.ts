export class Pd6SideBarConfig {

    pd6SidebarConfig: object[];

    constructor() {

    }

    getSideBarConfig(role: string) {
        if (role == "DS_PENSION_OFFICER") {
            this.pd6SidebarConfig = [
                {
                    "menuItem": "Search Pensioner",
                    "path": "/login/pd6-pensions/search-nic"
                }
                // {
                //     "menuItem": "Report",
                //     "path": "/login/pd6-pensions/report"
                // }
            ]
        }

    }
}